WordPress Nav Menu Widget Extension
===================================

Replaces WordPress custom menu widget by another one with more options:

![Widget Controls](screenshot-1.png)